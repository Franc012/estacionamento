#include <stdlib.h>
#include <stdio.h>

struct Carro {
    char placa[8];
    char modelo[30];
    char cor[15];
};

struct Pilha {
    struct Carro *dados;
    int topo;
    int capacidade;
};

struct Pilha *criarPilha(int capacidade) {
    struct Pilha *pilha = (struct Pilha *)malloc(sizeof(struct Pilha));
    if (pilha == NULL) {
        printf("Erro ao alocar memória para a pilha.\n");
        return NULL;
    }

    pilha->dados = (struct Carro *)malloc(capacidade * sizeof(struct Carro));
    if (pilha->dados == NULL) {
        printf("Erro ao alocar memória para os dados da pilha.\n");
        free(pilha);
        return NULL;
    }

    pilha->topo = -1; 
    pilha->capacidade = capacidade;
    return pilha;
}

void liberarPilha(struct Pilha *pilha) {
    if (pilha != NULL) {
        free(pilha->dados);
        free(pilha);
    }
}

int pilhaVazia(struct Pilha *pilha) {
    return pilha->topo == -1;
}

int pilhaCheia(struct Pilha *pilha) {
    return pilha->topo == pilha->capacidade - 1;
}

void inserirCarro(struct Pilha *pilha, struct Carro carro) {
    if (pilhaCheia(pilha)) {
        printf("Estacionamento lotado!\n");
        return;
    }

    pilha->topo++;
    pilha->dados[pilha->topo] = carro; 

    printf("Carro estacionado com sucesso!\n");
    printf("Placa: %s\n", carro.placa);
    printf("Modelo: %s\n", carro.modelo);
    printf("Cor: %s\n\n", carro.cor);
}

void removerCarro(struct Pilha *pilha) {
    if (pilhaVazia(pilha)) {
        printf("Estacionamento vazio!\n");
        return;
    }

    struct Carro carroRemovido = pilha->dados[pilha->topo];
    pilha->topo--;

    printf("Carro retirado do estacionamento!\n");
    printf("Placa: %s\n", carroRemovido.placa);
    printf("Modelo: %s\n", carroRemovido.modelo);
    printf("Cor: %s\n\n", carroRemovido.cor);
}

void exibirCarros(struct Pilha *pilha) {
    if (pilhaVazia(pilha)) {
        printf("Estacionamento vazio!\n");
        return;
    }

    printf("Carros estacionados (ordem de chegada):\n");
    for (int i = 0; i <= pilha->topo; i++) { 
        struct Carro carro = pilha->dados[i];
        printf("Placa: %s\n", carro.placa);
        printf("Modelo: %s\n", carro.modelo);
        printf("Cor: %s\n\n", carro.cor);
    }
}

int Menu() {
    int resp;

    printf("*********************************************************************\n");
    printf("*                      < Estacionamento >                           *\n");
    printf("* 1 - Estacionar carro               2 - Listar carros estacionados *\n");
    printf("* 3 - Remover carro estacionado      0 - Sair do sistema            *\n");
    printf("*********************************************************************\n\n");

    scanf("%d", &resp);
    while ((getchar()) != '\n'); 

    return resp;
}

void logic(struct Pilha *pilha, int resp) {
    struct Carro carro;
    int opt;

    switch (resp) {
    case 1:
        printf("Insira a placa do seu carro: \n");
        scanf("%7s", carro.placa);
        while ((getchar()) != '\n'); 
        printf("Insira o modelo do seu carro: \n");
        scanf("%29s", carro.modelo);
        while ((getchar()) != '\n'); 
        printf("Insira a cor do seu carro: \n");
        scanf("%14s", carro.cor);
        while ((getchar()) != '\n'); 
        inserirCarro(pilha, carro);
        break;
    case 2:
        exibirCarros(pilha);
        break;
    case 3:
        removerCarro(pilha);
        break;
    case 0:
        exit(0);
        break;
    default:
        printf("Operacao invalida! Escolha uma das opcoes\n");
        opt = Menu();
        logic(pilha, opt);
        break;
    }

    printf("***********************************************\n");
    printf("1 - voltar ao menu          0 - sair do sistema\n");
    scanf("%d", &opt);
    switch (opt) {
    case 1:
        logic(pilha, Menu());
        break;
    case 0:
        exit(0);
        break;
    default:
        printf("Operacao invalida! \n");
        break;
    }
}

int main() {
    int capacidade, escolha;
    capacidade = 5;

    struct Pilha *pilha = criarPilha(capacidade);

    escolha = Menu();
    logic(pilha, escolha);

    liberarPilha(pilha);

    return 0;
}